// Middleware in ra thời gian hiện tại
const getTimeCurrent = (req, res, next) => {
    console.log("Current Time: ", new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }));
    next();
}

// export middleware
module.exports = {
    getTimeCurrent
}